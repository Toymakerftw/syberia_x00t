LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := SnapdragonCamera
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := SnapdragonCamera/SnapdragonCamera.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_OVERRIDES_PACKAGES := Camera2
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

#PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,$LOCAL_PATH/prebuilt/blobs/system/lib,system/lib)

#PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,$LOCAL_PATH/prebuilt/blobs/system/lib64,system/lib64)
